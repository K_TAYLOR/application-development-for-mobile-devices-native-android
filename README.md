I created a mobile application that could be used as a rental apartment finder.

•	It allows the user to find, add and edit rental listings.

•	This a multi-platform app, engineered using PhoneGap software.

•	It was initially implemented as a native Android App (Android Studio developed) coded in Java.

I created a basic input screen to allow users enter descriptive information 
in the input fields such as, 
property type, bedrooms, date and time of adding property, monthly rent price, furniture type, 
notes and name of the original poster.

Implementation forms of validation were included 
to give a user an error message if information was not entered correctly, 
which disengages the process of the post being successfully added within the application.
Only when all information is correct, will it work. 

The details are stored on a SQL database (relational database management system), 
and can be viewed, deleted and checked for duplicate entries of which would automatically be removed.
A search function along with filters were added for users to be able to search for their desired property. 
A note input screen was also added to allow users to make a comment about a specific posting, 
either as a review of the tenant, property, or just general comments made relating to the post e.g. a forum. 

