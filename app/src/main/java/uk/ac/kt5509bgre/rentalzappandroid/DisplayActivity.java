package uk.ac.kt5509bgre.rentalzappandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class DisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        TextView propertyView= (TextView) findViewById(R.id.textViewProperty);
        propertyView.setText(getIntent().getExtras().getString("property"));

        TextView bedroomsView= (TextView) findViewById(R.id.textViewBedrooms);
        bedroomsView.setText(getIntent().getExtras().getString("bedrooms"));

        TextView furnitureView= (TextView) findViewById(R.id.textViewFurniture);
        furnitureView.setText(getIntent().getExtras().getString("furniture"));


        TextView dateView= (TextView) findViewById(R.id.textViewDate);
        dateView.setText(getIntent().getExtras().getString("date"));

        TextView timeView= (TextView) findViewById(R.id.textViewTime);
        timeView.setText(getIntent().getExtras().getString("time"));

        TextView nameView= (TextView) findViewById(R.id.textViewName);
        nameView.setText(getIntent().getExtras().getString("name"));

        TextView notesView= (TextView) findViewById(R.id.textViewNotes);
        notesView.setText(getIntent().getExtras().getString("notes"));

        TextView priceView= (TextView) findViewById(R.id.textViewPrice);
        priceView.setText("£"+ getIntent().getExtras().getString("price"));

        Button edit = (Button) findViewById(R.id.buttonEdit);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
