package uk.ac.kt5509bgre.rentalzappandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ListingActivity extends AppCompatActivity {

    private String selectedType1="";
    private String selectedType2="";
    private String selectedType3="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);


        final EditText name = (EditText) findViewById(R.id.editText5);
        final EditText price = (EditText) findViewById(R.id.editText3);
        final EditText date = (EditText) findViewById(R.id.editText1);
        final EditText time = (EditText) findViewById(R.id.editText2);
        final EditText notes = (EditText) findViewById(R.id.editText4);

        final RadioGroup rgroup1 = (RadioGroup) findViewById(R.id.RadioGroup1);
        final RadioButton rbutton1 = (RadioButton) findViewById(R.id.RadioButton1);
        final RadioButton rbutton2 = (RadioButton) findViewById(R.id.RadioButton2);
        final RadioButton rbutton3 = (RadioButton) findViewById(R.id.RadioButton3);
        final RadioButton rbutton4 = (RadioButton) findViewById(R.id.RadioButton4);
        final RadioButton rbutton5 = (RadioButton) findViewById(R.id.RadioButton5);

        rgroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                if (i==R.id.RadioButton1){
                    selectedType1 = rbutton1.getText().toString();
                }else if (i==R.id.RadioButton2){
                    selectedType1 = rbutton2.getText().toString();
                }else if (i==R.id.RadioButton3){
                    selectedType1 = rbutton3.getText().toString();
                }else if (i==R.id.RadioButton4){
                    selectedType1 = rbutton4.getText().toString();
                }else{
                    selectedType1 = rbutton5.getText().toString();
                }

            }
        });

        final RadioGroup rgroup2 = (RadioGroup) findViewById(R.id.RadioGroup2);
        final RadioButton rbutton6 = (RadioButton) findViewById(R.id.RadioButton6);
        final RadioButton rbutton7 = (RadioButton) findViewById(R.id.RadioButton7);
        final RadioButton rbutton8 = (RadioButton) findViewById(R.id.RadioButton8);
        final RadioButton rbutton9 = (RadioButton) findViewById(R.id.RadioButton9);
        final RadioButton rbutton10 = (RadioButton) findViewById(R.id.RadioButton10);
        final RadioButton rbutton11 = (RadioButton) findViewById(R.id.RadioButton11);

        rgroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                if (i==R.id.RadioButton6){
                    selectedType2 = rbutton6.getText().toString();
                }else if (i==R.id.RadioButton7){
                    selectedType2 = rbutton7.getText().toString();
                }else if (i==R.id.RadioButton8){
                    selectedType2 = rbutton8.getText().toString();
                }else if (i==R.id.RadioButton9){
                    selectedType2 = rbutton9.getText().toString();
                }else if (i==R.id.RadioButton10){
                    selectedType2 = rbutton10.getText().toString();
                }else{
                    selectedType2 = rbutton11.getText().toString();
                }
            }
        });

        final RadioGroup rgroup3 = (RadioGroup) findViewById(R.id.RadioGroup3);
        final RadioButton rbutton12 = (RadioButton) findViewById(R.id.RadioButton12);
        final RadioButton rbutton13 = (RadioButton) findViewById(R.id.RadioButton13);
        final RadioButton rbutton14 = (RadioButton) findViewById(R.id.RadioButton14);

        rgroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i)
            {
                if (i==R.id.RadioButton12){
                    selectedType3 = rbutton12.getText().toString();
                }else if (i==R.id.RadioButton13){
                    selectedType3 = rbutton13.getText().toString();
                }else{
                    selectedType3 = rbutton14.getText().toString();
                }

            }
        });



        Button submit = (Button) findViewById(R.id.button1);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)

            {
                if (rgroup1.getCheckedRadioButtonId()<=0) {
                    rbutton5.setError("Select property");
                    Toast.makeText(getApplicationContext(), "Select property", Toast.LENGTH_SHORT).show();
                }
                else if (date.getText().length() == 0) {
                    date.setError("Please enter date of listing");
                }
                else if (time.getText().length() == 0) {
                    time.setError("Please enter time of listing");
                }
                else if (rgroup2.getCheckedRadioButtonId()<=0) {
                    rbutton11.setError("Select amount of bedrooms");
                    Toast.makeText(getApplicationContext(), "Select amount of bedrooms", Toast.LENGTH_SHORT).show();
                }
                else if (price.getText().length() == 0) {
                    price.setError("Please enter a price");
                }
                else if (name.getText().length() == 0) {
                    name.setError("Please enter your name");
                }
                else {

                    Intent intent = new Intent(ListingActivity.this, DisplayActivity.class);

                    intent.putExtra("property", selectedType1);
                    intent.putExtra("date", date.getText().toString());
                    intent.putExtra("time", time.getText().toString());
                    intent.putExtra("bedrooms", selectedType2);
                    intent.putExtra("price", price.getText().toString());
                    intent.putExtra("furniture", selectedType3);
                    intent.putExtra("notes", notes.getText().toString());
                    intent.putExtra("name", name.getText().toString());

                    startActivity(intent);
                }

            }

        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
